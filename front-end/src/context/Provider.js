import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Context from './Context';

const Provider = ({ children }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cpf, setCpf] = useState('');
  const [fetching, setFetching] = useState(false);
  const [data, setData] = useState([]);
  const [search, setSearch] = useState([]);
  const [page, setPage] = useState('MainFood');
  const [details, setDetails] = useState(['data']);
  const [categories, setCategories] = useState([]);
  const [category, setCategory] = useState('');
  const [done, setDone] = useState(false);
  const [ongoing, setOngoing] = useState(['data']);
  const [isIngrFilter, setIsIngrFilter] = useState(false);
  const [nome, setName] = useState('');
  const [endereco, setEndereco] = useState('');
  const [admin, setAdmin] = useState(false);

  const context = {
    email,
    setEmail,
    cpf,
    setCpf,
    fetching,
    setFetching,
    data,
    setData,
    search,
    setSearch,
    page,
    setPage,
    details,
    setDetails,
    categories,
    setCategories,
    category,
    setCategory,
    done,
    setDone,
    ongoing,
    setOngoing,
    isIngrFilter,
    setIsIngrFilter,
    nome,
    setName,
    admin,
    setAdmin,
    endereco,
    setEndereco,
    password,
    setPassword
  };

  return <Context.Provider value={context}>{children}</Context.Provider>;
};

export default Provider;

Provider.propTypes = {
  children: PropTypes.objectOf(Object).isRequired,
};
