import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import './css/Login.css';

import Context from '../context/Context';

function inputEmail(handleEmailChange) {
  return (
    <div className="login-input">
      <input
        type="email"
        placeholder="Email"
        data-testid="email-input"
        name="email"
        onChange={(e) => handleEmailChange(e)}
        required
      />
    </div>
  );
}

function inputPassword(handlePasswordChange) {
  return (
    <div className="login-input">
      <input
        type="password"
        placeholder="Senha"
        data-testid="password-input"
        name="password"
        onChange={(e) => handlePasswordChange(e)}
        required
      />
    </div>
  );
}

function Login() {
  const [checkedEmail, setCheckedEmail] = useState(false);
  const [checkedPassword, setCheckedPassword] = useState(false);
  const { setEmail, setPassword, email } = useContext(Context);

  const checkEmail = (emailTested) => {
    const regexEmail = /^[\w+.]+@\w+\.\w{2,}(?:\.\w{2})?$/;
    if (regexEmail.test(emailTested) === true) setCheckedEmail(true);
  };
  const checkPassword = (passwordTested) => {
    if (passwordTested.length > 4) setCheckedPassword(true);
  };
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    checkEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
    checkPassword(e.target.value);
  };

  const storage = () => {
    localStorage.setItem('user', JSON.stringify({ email }));
  };

  return (
    <div className="login-page" data-testid="">
      {inputEmail(handleEmailChange)}
      {inputPassword(handlePasswordChange)}
      <Link className="enter" to="/">
        <button
          type="button"
          data-testid="login-submit-btn"
          disabled={!(checkedEmail && checkedPassword)}
          onClick={() => storage()}
        >
          Entrar
        </button>
      </Link>
    </div>
  );
}

export default Login;
