import React, { Component } from 'react';
import './css/NavBar.css';

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <nav className="menu-superior">
          <ul>
            <li>
              <a href="/">Lista Vacinados</a>
            </li>
            <li>
              <a href="/add-user/_add">Cadastrar</a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default NavBar;
