import React, { Component } from 'react';
import fetch from '../services/fetch';
import InputMask from 'react-input-mask';

import './css/CreateUser.css';

class CreateUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // step 2
      id: this.props.match.params.id,
      nome: '',
      cpf: '',
      email: '',
      endereco: {
        cep: '',
        rua: '',
        numero: '',
        bairro: '',
        cidade: '',
      },
    };
    this.changeName = this.changeName.bind(this);
    this.changeCpf = this.changeCpf.bind(this);
    this.changeEmail = this.changeEmail.bind(this);
    this.changeEndereco = this.changeEndereco.bind(this);
    this.changeCep = this.changeCep.bind(this);
    this.changeRua = this.changeRua.bind(this);
    this.changeNum = this.changeNum.bind(this);
    this.changeBairro = this.changeBairro.bind(this);
    this.changeCidade = this.changeCidade.bind(this);
    this.saveOrUpdateUser = this.saveOrUpdateUser.bind(this);
    // this.blurCep = this.blurCep.bind(this);
  }

  componentDidMount() {
    if (this.state.id === '_add') {
      return;
    } else {
      fetch.getUserById(this.state.id).then((res) => {
        let user = res.data;
        this.setState({
          nome: user.nome,
          cpf: user.cpf,
          email: user.email,
          endereco: {
            cep: user.endereco.cep,
            rua: user.endereco.rua,
            numero: user.endereco.numero,
            bairro: user.endereco.bairro,
            cidade: user.endereco.cidade,
          },
        });
      });
    }
  }
  saveOrUpdateUser = (e) => {
    e.preventDefault();
    let user = {
      nome: this.state.nome,
      cpf: this.state.cpf,
      email: this.state.email,
      endereco: {
        cep: this.state.endereco.cep,
        rua: this.state.endereco.rua,
        numero: this.state.endereco.numero,
        bairro: this.state.endereco.bairro,
        cidade: this.state.endereco.cidade,
      },
    };
    console.log('user => ' + JSON.stringify(user));

    if (this.state.id === '_add') {
      fetch.createUser(user).then((res) => {
        this.props.history.push('/');
      });
    } else {
      fetch.updateUser(user, this.state.id).then((res) => {
        this.props.history.push('/');
      });
    }
  };

  changeName = (event) => {
    this.setState({ nome: event.target.value });
  };

  changeCpf = (event) => {
    this.setState({ cpf: event.target.value });
  };

  changeEmail = (event) => {
    this.setState({ email: event.target.value });
  };

  changeEndereco = (event) => {
    this.setState({ endereco: event.target.value });
  };

  changeRua = (event) => {
    this.setState({ endereco: { ...this.state.endereco, rua: event.target.value } });
  };

  changeNum = (event) => {
    this.setState({ endereco: { ...this.state.endereco, numero: event.target.value } });
  };

  changeCep = (event) => {
    this.setState({ endereco: { ...this.state.endereco, cep: event.target.value } });
  };

  changeBairro = (event) => {
    this.setState({ endereco: { ...this.state.endereco, bairro: event.target.value } });
  };

  changeCidade = (event) => {
    this.setState({ endereco: { ...this.state.endereco, cidade: event.target.value } });
  };

  cancel() {
    this.props.history.push('/');
  }

  /* blurCep = (ev) => {
    const digitaCep = ev.target.value;
    fetch(`https://viacep.com.br/ws/${digitaCep}/json/`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);

        this.setState.endereco.rua = data.logradouro;
        this.setState.endereco.bairro = data.bairro;
        this.setState.endereco.cidade = data.localidade;
      });
  };
  */

  render() {
    return (
      <div>
        <br></br>
        <div className="container">
          <div>
            <div className="card-body">
              <form>
                <div className="formulario">
                  <label> Nome </label>
                  <input
                    type="text"
                    placeholder="Nome"
                    name="nome"
                    className="form-control"
                    value={this.state.nome}
                    onChange={this.changeName}
                    required
                  />
                </div>
                <div className="formulario">
                  <label> CPF </label>
                  <input
                    type="number"
                    placeholder="CPF"
                    name="cpf"
                    className="form-control"
                    value={this.state.cpf}
                    onChange={this.changeCpf}
                    required
                  />
                </div>
                <div className="formulario">
                  <label> Email </label>
                  <input
                    type="email"
                    placeholder="Email"
                    name="email"
                    className="form-control"
                    value={this.state.email}
                    onChange={this.changeEmail}
                    required
                  />
                </div>

                <div className="formulario">
                  <label> Endereço </label>
                  <InputMask
                    mask="99999-999"
                    type="text"
                    placeholder="cep"
                    name="cep"
                    className="form-control"
                    value={this.state.endereco.cep}
                    onChange={this.changeCep}
                    required
                  />
                  <input
                    type="text"
                    placeholder="rua"
                    name="rua"
                    className="form-control"
                    value={this.state.endereco.rua}
                    onChange={this.changeRua}
                    required
                  />
                  <input
                    type="number"
                    placeholder="numero"
                    name="numero"
                    className="form-control"
                    value={this.state.endereco.numero}
                    onChange={this.changeNum}
                    required
                  />
                  <input
                    type="text"
                    placeholder="bairro"
                    name="bairro"
                    className="form-control"
                    value={this.state.endereco.bairro}
                    onChange={this.changeBairro}
                    required
                  />
                  <input
                    type="text"
                    placeholder="cidade"
                    name="endereco"
                    className="form-control"
                    value={this.state.endereco.cidade}
                    onChange={this.changeCidade}
                    required
                  />
                </div>

                <button className="btn-save" onClick={this.saveOrUpdateUser}>
                  Salvar
                </button>
                <button className="btn-cancel" onClick={this.cancel.bind(this)}>
                  Cancelar
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateUser;
