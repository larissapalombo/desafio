import React, { Component } from 'react';
import fetch from '../services/fetch';

class ViewUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      user: {},
    };
  }

  componentDidMount() {
    fetch.getUserById(this.state.id).then((res) => {
      this.setState({ user: res.data });
    });
  }

  render() {
    return (
      <div>
        <br></br>
        <div className="card col-md-6 offset-md-3">
          <div className="card-body">
            <div className="row">
              <label> Nome </label>
              <div> {this.state.user.nome}</div>
            </div>
            <div className="row">
              <label> CPF </label>
              <div> {this.state.user.cpf}</div>
            </div>
            <div className="row">
              <label> Email </label>
              <div> {this.state.user.email}</div>
            </div>
            <div className="row">
              <label> Endereco </label>
              <div> {this.state.user.endereco}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ViewUser;
