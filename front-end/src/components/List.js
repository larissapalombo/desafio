import React, { Component } from 'react';
import fetch from '../services/fetch';

import './css/ListUser.css';

class ListUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      usuarios: [],
    };
    this.addUser = this.addUser.bind(this);
    this.editUser = this.editUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
  }

  deleteUser(id) {
    fetch.deleteUser(id).then((res) => {
      this.setState({ usuarios: this.state.usuarios.filter((usuario) => usuario.id !== id) });
    });
  }
  viewUser(id) {
    this.props.history.push(`/view-user/${id}`);
  }
  editUser(id) {
    this.props.history.push(`/add-user/${id}`);
  }

  componentDidMount() {
    fetch.getUser().then((res) => {
      this.setState({ usuarios: res.data });
    });
  }

  addUser() {
    this.props.history.push('/add-user/_add');
  }

  render() {
    return (
      <div className="container">
        <div className="table-responsive">
          <table>
            <thead>
              <tr>
                <th> Nome </th>
                <th> CPF </th>
                <th> Email </th>
                <th> Cidade </th>
              </tr>
            </thead>
            <tbody>
              {this.state.usuarios.map((user) => (
                <tr key={user.id}>
                  <td> {user.nome} </td>
                  <td> {user.cpf}</td>
                  <td> {user.email}</td>
                  <td> {user.endereco.cidade}</td>
                  <td>
                    <button onClick={() => this.editUser(user.id)} className="btn-edit">
                      Atualizar{' '}
                    </button>
                  </td>
                  <td>
                    <button onClick={() => this.deleteUser(user.id)} className="btn-del">
                      Excluir{' '}
                    </button>
                  </td>
                  <td>
                    <button onClick={() => this.viewUser(user.id)} className="btn-view">
                      Ver{' '}
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ListUser;
