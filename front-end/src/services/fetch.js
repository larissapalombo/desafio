import axios from 'axios';

const URL = "http://localhost:5000/usuarios";

class fetch {

    getUser(){
        return axios.get(URL);
    }

    createUser(user){
        return axios.post(URL, user);
    }

    getUserById(userId){
        return axios.get(URL + '/' + userId);
    }

    updateUser(user, userId){
        return axios.put(URL + '/' + userId, user);
    }

    deleteUser(userId){
        return axios.delete(URL + '/' + userId);
    }
}

export default new fetch();
