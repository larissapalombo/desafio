import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/Login';
import NavBar from './components/NavBar';
import List from './components/List';
import CreateUser from './components/CreateUser';
import ViewUser from './components/ViewUser';

const App = () => {
  return (
    <div>
      <NavBar />
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route path="/" exact component={List} />
          <Route path="/add-user/:id" component={CreateUser} />
          <Route path="/view-user/:id" component={ViewUser} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
